1) Need to modify %appdata%\NuGet\NuGet.Config file, add following key:

  <packageSources>
    ...
    <add key="net5-sdk-il" value="https://dotnet.myget.org/feed/dotnet-core/package/nuget/Microsoft.NET.Sdk.IL" />
    ...
  </packageSources>


2) Add global.json to the root of you IL projects:

{
  "msbuild-sdks": 
  {
    "Microsoft.NET.Sdk.IL": "5.0.0"
  }
}  